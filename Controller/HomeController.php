<?php
class HomeController extends AppController
{
    public function index(){
        $this->layout = 'home';
        session_start();
        if (!isset($_SESSION['user_id'])){
            $this->redirect('/users/login');
        }
    }

    public function timeline(){
        $this->layout = 'home_cal';

    }
    public function nothing(){

    }
}

