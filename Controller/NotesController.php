<?php
class NotesController extends AppController{

    public $helpers = array('Html', 'Form');

    public function index(){
        $this->layout = 'home';
        $daily = $this->Note->find('all', array(
            'order' => array('Note.date' => 'asc'),
            'conditions' => array('Note.date')
        ));
        $this->set('dailys', $daily);
    }

    public function add(){
        $this->layout = 'home';
        if ($this->request->is('ajax')) {
            $data = $this->request->data;
            if ($this->Note->save($data)){
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }
    }

    public function all(){
        $this->layout = 'home';
        $data = $this->Note->find('all', array(
            'order' => array('Note.date' => 'asc')
        ));
        $this->set('datas', $data);
    }

    public function edit($id) {
        $this->layout = 'home';
        // Has any form data been POSTed?
        if ($this->request->is('post') || $this->request->is('ajax')) {
            // If the form data can be validated and saved...
            if ($this->Note->save($this->request->data)) {
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }
        // If no form data, find the recipe to be edited
        // and hand it to the view.
        $this->set('data', $this->Note->findById($id));
    }

    public function delete(){
        $this->layout = false;
        $this->autoRender = false;
        $this->loadModel('Note');
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $id = $this->request->data('id');
        if ($this->Note->delete($id)) {
            echo json_encode(array('result' => 'true'));
        } else {
            echo json_encode(array('result' => 'false'));
        }
    }



    /*
     * API Function Call Here
     */
    public function index_api(){
        $this->layout = false;
        $data = $this->Note->find('all');
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function add_api(){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Note->save($data)){
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function view_api($id){
        $this->layout = false;
        $data = $this->Note->findById($id);
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function update_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Note->findById($id)){
            $this->Note->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->Note->set($key, $value);
            }
            try {
                if ($this->Note->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function delete_api($id){
        $this->layout = false;
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Note->delete($id)) {
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function edit_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Note->findById($id)){
            $this->Note->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->Note->set($key, $value);
            }
            try {
                if ($this->Note->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

}