<?php
    class TasksController extends AppController{
        public $helpers = array('Html', 'Form');

        public function index(){
            $this->layout = 'home';
            $daily = $this->Task->find('all', array(
                'order' => array('Task.deadline' => 'asc')
            ));
            $this->set('dailys', $daily);
        }

        public function add(){
            $this->layout = 'home';
            if ($this->request->is('ajax')) {
                $data = $this->request->data;
                if ($this->Task->save($data)){
                    echo json_encode(array('result' => 'true'));
                } else {
                    echo json_encode(array('result' => 'false'));
                }
            }
        }

        public function edit($id) {
            $this->layout = 'home';
            // Has any form data been POSTed?
            if ($this->request->is('post') || $this->request->is('ajax')) {
                // If the form data can be validated and saved...
                if ($this->Task->save($this->request->data)) {
                    echo json_encode(array('result' => 'true'));
                } else {
                    echo json_encode(array('result' => 'false'));
                }
            }
            // If no form data, find the recipe to be edited
            // and hand it to the view.
            $this->set('data', $this->Task->findById($id));
        }
        
        public function delete(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('Task');
            if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            $id = $this->request->data('id');
            if ($this->Task->delete($id)) {
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }


        /*
        * API Function Call Here
        */
        public function index_api(){
            $this->layout = false;
            $data = $this->Task->find('all');
            $result = json_encode($data);
            $this->set('result' , $result);
        }

        public function add_api(){
            $this->layout = false;
            $data = $this->request->data;
            if ($this->Task->save($data)){
                $result = json_encode(array('result' => 'true'));
                $this->set('result', $result);
            } else {
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        }

        public function view_api($id){
            $this->layout = false;
            $data = $this->Task->findById($id);
            $result = json_encode($data);
            $this->set('result' , $result);
        }

        public function update_api($id){
            $this->layout = false;
            $data = $this->request->data;
            if ($this->Task->findById($id)){
                $this->Task->read(null ,$id);
                foreach ($data as $key=>$value){
                    $this->Task->set($key, $value);
                }
                try {
                    if ($this->Task->save()){
                        $result = json_encode(array('result' => 'true'));
                        $this->set('result', $result);
                    } else {
                        $result = json_encode(array('result' => 'false'));
                        $this->set('result', $result);
                    }
                } catch (Exception $e){
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } else {
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        }

        public function delete_api($id){
            $this->layout = false;
            if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            if ($this->Task->delete($id)) {
                $result = json_encode(array('result' => 'true'));
                $this->set('result', $result);
            } else {
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        }

        public function edit_api($id){
            $this->layout = false;
            $data = $this->request->data;
            if ($this->Task->findById($id)){
                $this->Task->read(null ,$id);
                foreach ($data as $key=>$value){
                    $this->Task->set($key, $value);
                }
                try {
                    if ($this->Task->save()){
                        $result = json_encode(array('result' => 'true'));
                        $this->set('result', $result);
                    } else {
                        $result = json_encode(array('result' => 'false'));
                        $this->set('result', $result);
                    }
                } catch (Exception $e){
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } else {
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        }


    }