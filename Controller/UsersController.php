<?php

class UsersController extends AppController{

	public $helpers = array('Html', 'Form');

	public $component = array('Auth');

	public function beforeFilter() {
    	parent::beforeFilter();
    // Allow users to register and logout.
        $a = $this->Auth;
        if (isset($this->Auth)){
            $this->Auth->allow('signup', 'logout');
        }
	}
    // Login to server
	public function login() {
		$this->layout = 'form_layout';
		if (isset($_SESSION['user_id'])){
		    $this->redirect('/');
        }
		if ($this->request->is('post')) {
		    $data = $this->request->data;
		    $this->User->create();
		    $isExist = $this->User->findByUsername($data['User']['username']);
		    if ($isExist){
		        if ($data['User']['password'] == $isExist['User']['password'])
                {
                    session_start();
                    $_SESSION['user_id'] = $isExist['User']['id'];
                    $_SESSION['user_name'] = $data['User']['username'];
                    $this->redirect('/');
                } else {
                    echo json_encode('false');
		        }
            }
    	}
	}

	public function logout() {
		$this->layout = 'form_layout';
		if (isset($_SESSION['user_id'])){
            session_destroy();
        }
    	return $this->redirect('/users/login');
	}

    public function signup() {
		$this->layout = 'form_layout';
		if($this->request->is('post')) {
			$this->User->create();
			$this->User->findByUsername($this->request->data['User']['username']);
            if(!$this->User->findByUsername($this->request->data['User']['username'])) {
				if ($this->User->save($this->request->data)) {
//               		$this->Session->setFlash('Signed Up Successfully');
                	return $this->redirect(array('action' => 'login'));
            	} else {
//        	 		$this->Session->setFlash('Signed in Failed');
            	}
			} else {
//			     $this->Session->setFlash('Username is already existed');
			}
		}
	}

    /*
     * API Function Call Here
     */
    public function index_api(){
        $this->layout = false;
        $data = $this->User->find('all');
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function add_api(){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->User->save($data)){
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function view_api($id){
        $this->layout = false;
        $data = $this->User->findById($id);
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function update_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->User->findById($id)){
            $this->User->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->User->set($key, $value);
            }
            try {
                if ($this->User->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function delete_api($id){
        $this->layout = false;
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->User->delete($id)) {
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function edit_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->User->findById($id)){
            $this->User->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->User->set($key, $value);
            }
            try {
                if ($this->User->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

}
