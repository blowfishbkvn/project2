<?php

App::import('vendors', 'google');


class TimetablesController extends AppController{

    public $helpers = array('Html', 'Form');

    public function index(){
        $this->layout = 'home';
        $data = $this->Timetable->find('all', array(
            'order' => array('Timetable.day' => 'asc')
        ));
        $this->set('datas', $data);
    }

    public function gridview(){
        $this->layout = 'home';
        $data = $this->Timetable->find('all', array(
            'order' => array('Timetable.day' => 'asc')
        ));
        $this->set('datas', $data);
    }

    public function add(){
        $this->layout = 'home';
        if ($this->request->is('ajax')) {
            $data = $this->request->data;
            try {
                if ($this->Timetable->save($data)){
                    echo json_encode(array('result' => 'true'));
                } else {
                    echo json_encode(array('result' => 'false'));
                }
            } catch (Exception $e){
                echo json_encode(array('result' => 'false'));
            }

        }
    }

    public function edit($id) {
        $this->layout = 'home';
        // Has any form data been POSTed?
        if ($this->request->is('post') || $this->request->is('ajax')) {
            // If the form data can be validated and saved...
            if ($this->Timetable->save($this->request->data)) {
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }
        // If no form data, find the recipe to be edited
        // and hand it to the view.
        $this->set('data', $this->Timetable->findById($id));
    }

    public function save(){
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $newData = array('Timetable'=>$this->request->data);
            if ($this->Timetable->save($newData)){
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }
    }

    public function delete(){
        $this->layout = false;
        $this->autoRender = false;
        $this->loadModel('Timetable');
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $id = $this->request->data('id');
        if ($this->Timetable->delete($id)) {
            echo json_encode(array('result' => 'true'));
        } else {
            echo json_encode(array('result' => 'false'));
        }
    }

    /*
    * API Function Call Here
    */
    public function index_api(){
        $this->layout = false;
        $data = $this->Timetable->find('all');
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function add_api(){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Timetable->save($data)){
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function view_api($id){
        $this->layout = false;
        $data = $this->Timetable->findById($id);
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function update_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Timetable->findById($id)){
            $this->Timetable->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->Timetable->set($key, $value);
            }
            try {
                if ($this->Timetable->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function delete_api($id){
        $this->layout = false;
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Timetable->delete($id)) {
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function edit_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Timetable->findById($id)){
            $this->Timetable->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->Timetable->set($key, $value);
            }
            try {
                if ($this->Timetable->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

}