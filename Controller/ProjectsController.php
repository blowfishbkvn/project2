<?php
class ProjectsController extends AppController{

    public $helpers = array('Html', 'Form');

    public function index($id = null){
        $this->layout = 'home';
        $projectList = $this->Project->find('all', array(
            'fields' => array('DISTINCT (Project.project_id)', 'Project.project_name'),
            'recursive' => 0
        ));
        if ($id == null){
            $projectId  = $projectList['0']['Project']['project_id'];
        } else {
            $projectId = $id;
        }
        $this->set('projectId', $projectId);
        $this->set('projectList', $projectList);
        $projectTask = $this->Project->find('all', array(
            'conditions' => array('Project.project_id' => $projectId)
        ));
        $this->set('projectTask', $projectTask);
    }

    public function add(){
        $this->layout = 'home';
        if ($this->request->is('ajax')) {
            $data = $this->request->data;
            if ($this->Project->save($data)){
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }
    }

    public function edit($id) {
        $this->layout = 'home';
        // Has any form data been POSTed?
        if ($this->request->is('post') || $this->request->is('ajax')) {
            // If the form data can be validated and saved...
            if ($this->Project->save($this->request->data)) {
                echo json_encode(array('result' => 'true'));
            } else {
                echo json_encode(array('result' => 'false'));
            }
        }
        // If no form data, find the recipe to be edited
        // and hand it to the view.
        $this->set('data', $this->Project->findById($id));
    }

    public function delete(){
        $this->layout = false;
        $this->autoRender = false;
        $this->loadModel('Project');
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $id = $this->request->data('id');
        if ($this->Project->delete($id)) {
            echo json_encode(array('result' => 'true'));
        } else {
            echo json_encode(array('result' => 'false'));
        }
    }


    /*
     * API Function Call Here
     */
    public function index_api(){
        $this->layout = false;
        $data = $this->Project->find('all');
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function add_api(){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Project->save($data)){
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function view_api($id){
        $this->layout = false;
        $data = $this->Project->findById($id);
        $result = json_encode($data);
        $this->set('result' , $result);
    }

    public function update_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Project->findById($id)){
            $this->Project->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->Project->set($key, $value);
            }
            try {
                if ($this->Project->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function delete_api($id){
        $this->layout = false;
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Project->delete($id)) {
            $result = json_encode(array('result' => 'true'));
            $this->set('result', $result);
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

    public function edit_api($id){
        $this->layout = false;
        $data = $this->request->data;
        if ($this->Project->findById($id)){
            $this->Project->read(null ,$id);
            foreach ($data as $key=>$value){
                $this->Project->set($key, $value);
            }
            try {
                if ($this->Project->save()){
                    $result = json_encode(array('result' => 'true'));
                    $this->set('result', $result);
                } else {
                    $result = json_encode(array('result' => 'false'));
                    $this->set('result', $result);
                }
            } catch (Exception $e){
                $result = json_encode(array('result' => 'false'));
                $this->set('result', $result);
            }
        } else {
            $result = json_encode(array('result' => 'false'));
            $this->set('result', $result);
        }
    }

}