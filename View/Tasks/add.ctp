<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Add New Tasks</h3>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tasks fa-fw"></i> New Tasks
                <div class="pull-right">
                    <a href="/tasks/index" class="btn btn-info active" role="button" style="margin-top: -7px;">
                        View Tasks
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputName" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputContent" class="col-sm-2 control-label">Content</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputContent" placeholder="Content">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDeadline" class="col-sm-2 control-label">Deadline</label>
                        <div class="col-sm-10">
                            <input type="datetime-local" class="form-control" id="inputDeadline" placeholder="Deadline">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPriority" class="col-sm-2 control-label">Priority</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPriority" placeholder="Priority">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 btn-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button onclick="addNewTasks()" class="btn btn-primary" style="margin-left: 10px">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function addNewTasks() {
        var name = $('#inputName').val();
        var content = $('#inputContent').val();
        var deadline = $('#inputDeadline').val();
        var priority = $('#inputPriority').val();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '<?php echo Router::url(array('controller' => 'tasks', 'action' => 'add'));?>',
            data: {
                'name': name,
                'content': content,
                'deadline': deadline,
                'priority': priority,
                'is_done': false
            },
            success: function(data) {
                if (data.result === 'true'){
                    $('#notifications').html('<p class=\"text-success\" style=\"margin-left: 15px; font-size: 200%;\">You have been added record successfully!!!</p>');
                } else {
                    $('#notifications').html('<p class=\"text-warning\">Something went wrong. Please try again !!</p>');
                }
            }
        });
    }
</script>
