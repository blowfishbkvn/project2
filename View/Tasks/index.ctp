<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">My Tasks</h1>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Task Of Mine
                <div class="pull-right">
                    <div class="panel-button">
                        <a href="#" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                            View All Tasks
                        </a>
                        <a href="/tasks/add" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                            Add New Tasks
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Today Tasks
            </div>
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Required Today
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a>
                                    </li>
                                    <li><a href="#">Another action</a>
                                    </li>
                                    <li><a href="#">Something else here</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <p class="text-danger">Task 1</p>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Incoming Tasks
            </div>
            <div class="panel-body">

            </div>
        </div>
    </div>
</div>
