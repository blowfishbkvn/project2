<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Add New Project</h3>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tasks fa-fw"></i> New Project
                <div class="pull-right">
                    <a href="/projects/index" class="btn btn-info active" role="button" style="margin-top: -7px;">
                        Back
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" id="inputName" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputContent" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputContent" placeholder="Content">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 btn-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button onclick="addNewProjects()" class="btn btn-primary" style="margin-left: 10px">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function addNewProjects() {
        var name = $('#inputName').val();
        var content = $('#inputContent').val();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '<?php echo Router::url(array('controller' => 'projects', 'action' => 'add'));?>',
            data: {
                'project_id': <?php echo date('U');?>,
                'project_name': name,
                'project_des': content
            },
            success: function(data) {
                if (data.result === 'true'){
                    $('#notifications').html('<p class=\"text-success\" style=\"margin-left: 15px; font-size: 200%;\">You have been added record successfully!!!</p>');
                } else {
                    $('#notifications').html('<p class=\"text-warning\">Something went wrong. Please try again !!</p>');
                }
            }
        });
    }
</script>
