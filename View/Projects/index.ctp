<?php
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header panel-body">My Project</h1>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <ul class="nav nav-tabs" role="tablist">
                <?php
                    foreach ($projectList as $aProject)
                    {
                        if ($projectId == $aProject['Project']['project_id']){
                            echo "<li class=\"active\" id='projectOf" . $aProject['Project']['project_id'] . "'><a href=\"/projects/index/" . $aProject['Project']['project_id'] . "\">" . $aProject['Project']['project_name'] . "</a></li>";
                        }else{
                            echo "<li id='projectOf" . $aProject['Project']['project_id'] . "'><a href=\"/projects/index/" . $aProject['Project']['project_id'] . "\">" . $aProject['Project']['project_name'] . "</a></li>";
                        }
                    }
                ?>
                <li class="pull-right">
                    <a href="/projects/add" class="btn btn-primary active" role="button" style="font-size: 90%">
                        Add New Project
                    </a>
                </li>
            </ul>
            <div class="panel-body" id="project-content">
                <div class="col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="text-center">Resource</span>
                            <button class="pull-right btn btn-primary active" role="button" style="margin-top: -8px; margin-right: -10px; ">
                                Add
                            </button>
                        </div>
                        <div class="panel-body" style="height: 550px; overflow: auto; background-color: #D1F0C7;">
                            <?php
                                foreach ($projectTask as $aTask){
                                    if ($aTask['Project']['name'] == null){
                                        continue;
                                    }
                                    if ($aTask['Project']['state'] == 0){
                                        echo "<div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    ". $aTask['Project']['name'] ."
                                    <div class=\"pull-right\">
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                Actions
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li><a href=\"#\">Move to Todo</a>
                                                </li>
                                                <li><a href=\"#\">Move to InProgress</a>
                                                </li>
                                                <li><a href=\"#\">Move to Done</a>
                                                </li>
                                                <li><a href=\"#\">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class=\"panel-body\">
                                    <p class=\"text-danger\">". $aTask['Project']['content'] ."</p>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            ";
                                    }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="text-center">Todo</span>
                            <button class="pull-right btn btn-primary active" role="button" style="margin-top: -8px; margin-right: -10px;">
                                Add
                            </button>
                        </div>
                        <div class="panel-body" style="height: 550px; overflow: auto; background-color: #D1F0C7;">
                            <?php
                            foreach ($projectTask as $aTask){
                                if ($aTask['Project']['state'] == 1){
                                    echo "<div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    ". $aTask['Project']['name'] ."
                                    <div class=\"pull-right\">
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                Actions
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li><a href=\"#\">Move to Resource</a>
                                                </li>
                                                <li><a href=\"#\">Move to InProgress</a>
                                                </li>
                                                <li><a href=\"#\">Move to Done</a>
                                                </li>
                                                <li><a href=\"#\">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class=\"panel-body\">
                                    <p class=\"text-danger\">". $aTask['Project']['content'] ."</p>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            ";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="text-center">In Progress</span>
                            <button class="pull-right btn btn-primary active" role="button" style="margin-top: -8px; margin-right: -10px;">
                                Add
                            </button>
                        </div>
                        <div class="panel-body" style="height: 550px; overflow: auto; background-color: #D1F0C7;">
                            <?php
                            foreach ($projectTask as $aTask){
                                if ($aTask['Project']['state'] == 2){
                                    echo "<div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    ". $aTask['Project']['name'] ."
                                    <div class=\"pull-right\">
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                Actions
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li><a href=\"#\">Move to Resource</a>
                                                </li>
                                                <li><a href=\"#\">Move to Todo</a>
                                                </li>
                                                <li><a href=\"#\">Move to Done</a>
                                                </li>
                                                <li><a href=\"#\">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class=\"panel-body\">
                                    <p class=\"text-danger\">". $aTask['Project']['content'] ."</p>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            ";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="text-center">Done</span>
                            <button class="pull-right btn btn-primary active" role="button" style="margin-top: -8px; margin-right: -10px;">
                                Add
                            </button>
                        </div>
                        <div class="panel-body" style="height: 550px; overflow: auto; background-color: #D1F0C7;">
                            <?php
                            foreach ($projectTask as $aTask){
                                if ($aTask['Project']['state'] == 3){
                                    echo "<div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    ". $aTask['Project']['name'] ."
                                    <div class=\"pull-right\">
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                Actions
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li><a href=\"#\">Move to Resource</a>
                                                </li>
                                                <li><a href=\"#\">Move to Todo</a>
                                                </li>
                                                <li><a href=\"#\">Move to InProgress</a>
                                                </li>
                                                <li><a href=\"#\">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class=\"panel-body\">
                                    <p class=\"text-danger\">". $aTask['Project']['content'] ."</p>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            ";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>