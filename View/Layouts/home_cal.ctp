<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>My TO-DO List</title>

    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css('vendor/bootstrap/css/bootstrap.min'); ?>

    <!-- MetisMenu CSS -->
    <?php echo $this->Html->css('vendor/metisMenu/metisMenu.min'); ?>

    <!-- Custom CSS -->
    <?php echo $this->Html->css('sb-admin-2'); ?>

    <!-- Custom Fonts -->
    <?php echo $this->Html->css('vendor/font-awesome/css/font-awesome.min'); ?>

<!--    --><?php //echo $this->Html->script('week/libs/jquery-1.4.4.min'); ?>
<!--    --><?php //echo $this->Html->script('week/libs/jquery-ui-1.8.11.custom.min'); ?>
        <?php echo $this->Html->script('weekcalender/jquery.min'); ?>

    <?php echo $this->Html->script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv'); ?>
    <?php echo $this->Html->script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min'); ?>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="../../img/todolist.png" style="width: inherit; height: inherit; margin-top: -15px;"></a>
        </div>
        <!-- /.navbar-header -->
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope fa-fw" style="font-size: 140%;"></i> <i class="fa fa-caret-down" style="font-size: 140%;"></i>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                <span class="pull-right text-muted">
                                        <em>08.pm</em>
                                    </span>
                            </div>
                            <div>Dinner meeting at 8.pm tomorrow.</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>Sara Tim</strong>
                                <span class="pull-right text-muted">
                                        <em>06.pm</em>
                                    </span>
                            </div>
                            <div>Can I have a little time of you tomorrow.</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>Read All Messages</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-tasks fa-fw" style="font-size: 140%;"></i> <i class="fa fa-caret-down" style="font-size: 140%;"></i>
                </a>
                <ul class="dropdown-menu dropdown-tasks">
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Project 2</strong>
                                    <span class="pull-right text-muted">96% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%">
                                        <span class="sr-only">96% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Tasks</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw" style="font-size: 140%;"></i> <i class="fa fa-caret-down" style="font-size: 140%;"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/users/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="/home/timeline"><i class="fa fa-table fa-fw"></i> Calender</a>
                    </li>
                    <li>
                        <a href="/timetables/index"><i class="fa fa-edit fa-fw"></i> TimeTables</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Events<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/tasks/index"> Tasks</a>
                            </li>
                            <li>
                                <a href="/notes/index"> Notes</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="/projects/index"><i class="fa fa-edit fa-fw"></i> Projects</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <?php echo $this->fetch('content'); ?>
    </div>

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<?php echo $this->Html->script('vendor/jquery/jquery.min'); ?>

<!-- Bootstrap Core JavaScript -->
<?php echo $this->Html->script('vendor/bootstrap/js/bootstrap.min'); ?>

<!-- Metis Menu Plugin JavaScript -->
<?php echo $this->Html->script('vendor/metisMenu/metisMenu.min'); ?>

<!-- Custom Theme JavaScript -->
<?php echo $this->Html->script('sb-admin-2'); ?>


</body>

</html>
