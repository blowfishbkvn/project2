<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>My TO-DO List</title>

    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css('vendor/bootstrap/css/bootstrap.min'); ?>

    <!-- MetisMenu CSS -->
    <?php echo $this->Html->css('vendor/metisMenu/metisMenu.min'); ?>

    <!-- Custom CSS -->
    <?php echo $this->Html->css('sb-admin-2'); ?>

    <!-- Custom Fonts -->
    <?php echo $this->Html->css('vendor/font-awesome/css/font-awesome.min'); ?>
    <?php echo $this->Html->css('iFormCss'); ?>
    <?php echo $this->Html->script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv'); ?>
    <?php echo $this->Html->script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min'); ?>

</head>

<body>

<div class="container" style="position: relative">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<?php echo $this->Html->script('vendor/jquery/jquery.min'); ?>

<!-- Bootstrap Core JavaScript -->
<?php echo $this->Html->script('vendor/bootstrap/js/bootstrap.min'); ?>

<!-- Metis Menu Plugin JavaScript -->
<?php echo $this->Html->script('vendor/metisMenu/metisMenu.min'); ?>

<?php echo $this->Html->script('vendor/morrisjs/morris.min'); ?>

<!-- Custom Theme JavaScript -->
<?php echo $this->Html->script('sb-admin-2'); ?>

</body>

</html>
