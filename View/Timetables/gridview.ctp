<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">My Timetables</h1>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Timetables
                <div class="pull-right">
                    <a href="/timetables/index" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                        Back
                    </a>
                    <a href="/timetables/add" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                        Add New
                    </a>
                </div>
            </div>
            <div class="panel-body" id="timetable-content">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Day</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Room</th>
                        <th>Class ID</th>
                        <th>Type</th>
                        <th>SubjectID</th>
                        <th>Subject Name</th>
                        <th>Notes</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($datas as $data): ?>
                        <tr id="timetable_record_<?php echo $data['Timetable']['id']; ?>">
                            <th> <?php echo $data['Timetable']['day']; ?> </th>
                            <th> <?php echo $data['Timetable']['start']; ?> </th>
                            <th> <?php echo $data['Timetable']['end']; ?> </th>
                            <th> <?php echo $data['Timetable']['room']; ?> </th>
                            <th> <?php echo $data['Timetable']['class_id']; ?> </th>
                            <th> <?php echo $data['Timetable']['type']; ?> </th>
                            <th> <?php echo $data['Timetable']['subject_id']; ?> </th>
                            <th> <?php echo $data['Timetable']['subject_name']; ?> </th>
                            <th> <?php echo $data['Timetable']['notes']; ?> </th>
                            <th>
                                <div class="btn-group">
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        <li><a href="<?php echo Router::url(array('controller' => 'timetables', 'action' => 'edit', $data['Timetable']['id']));?>">Edit</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)" onclick="deleteRecord(<?php echo $data['Timetable']['id']; ?>)">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </th>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function deleteRecord(id) {
        $.ajax({
            dataType: "json",
            type: "POST",
            showLoader: true,
            url: '<?php echo Router::url(array('controller' => 'timetables', 'action' => 'delete'));?>',
            data: {
                'id': id
            },
            success: function(data) {
                console.log(data);
                if (data.result === 'true'){
                    $('#notifications').html('<p class=\"text-success\" style=\"margin-left: 15px; font-size: 200%;\">You have been deleted record with id = ' + id + ' successfully!!!</p>');
                    $('#timetable_record_'+id).remove();
                } else {
                    $('#notifications').html('<p class=\"text-warning\">Something went wrong. Please try again !!</p>');
                }
            }
        });
    }
</script>