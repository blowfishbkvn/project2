<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Edit Your Timetables</h3>
    </div>
</div>
<div class="row" id="notifications">
    <?php
        if ($data === false){
            echo ("<p class=\"text-warning\">Something went wrong. Please try again later!!</p>");
    ?>
        </div>
        <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tasks fa-fw"></i> Edit Timetables
                <div class="pull-right">
                    <a href="/timetables/index" class="btn btn-info active" role="button" style="margin-top: -7px;">
                        View Timetable
                    </a>
                </div>
            </div>
            <div class="panel-body" id="timetable-add">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="inputDays" class="col-sm-2 control-label">Day</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputDays" placeholder="Days">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputStart" class="col-sm-2 control-label">Start</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputStart" placeholder="Start time">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEnd" class="col-sm-2 control-label">End</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEnd" placeholder="End time">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputRoom" class="col-sm-2 control-label">Room</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputRoom" placeholder="Room">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputClassId" class="col-sm-2 control-label">Class ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputClassId" placeholder="Class ID">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputType" class="col-sm-2 control-label">Type</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputType" placeholder="Type of Class">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSubjectId" class="col-sm-2 control-label">Subject ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSubjectId" placeholder="Subject ID">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSubjectName" class="col-sm-2 control-label">Subject Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSubjectName" placeholder="Subject Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNotes" class="col-sm-2 control-label">Notes</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputNotes" placeholder="Notes">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 btn-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button onclick="addnew()" class="btn btn-primary" style="margin-left: 10px">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <?php } else { ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-tasks fa-fw"></i> Edit Timetables
                        <div class="pull-right">
                            <a href="/timetables/index" class="btn btn-info active" role="button" style="margin-top: -7px;">
                                View Timetable
                            </a>
                        </div>
                    </div>
                    <div class="panel-body" id="timetable-add">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputDays" class="col-sm-2 control-label">Day</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputDays" placeholder="Days" value="<?php echo $data['Timetable']['day'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputStart" class="col-sm-2 control-label">Start</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputStart" placeholder="Start time" value="<?php echo $data['Timetable']['start'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEnd" class="col-sm-2 control-label">End</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputEnd" placeholder="End time" value="<?php echo $data['Timetable']['end'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputRoom" class="col-sm-2 control-label">Room</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputRoom" placeholder="Room" value="<?php echo $data['Timetable']['room'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputClassId" class="col-sm-2 control-label">Class ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputClassId" placeholder="Class ID" value="<?php echo $data['Timetable']['class_id'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputType" class="col-sm-2 control-label">Type</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputType" placeholder="Type of Class" value="<?php echo $data['Timetable']['type'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSubjectId" class="col-sm-2 control-label">Subject ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputSubjectId" placeholder="Subject ID"  value="<?php echo $data['Timetable']['subject_id'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSubjectName" class="col-sm-2 control-label">Subject Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputSubjectName" placeholder="Subject Name" value="<?php echo $data['Timetable']['subject_name'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNotes" class="col-sm-2 control-label">Notes</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputNotes" placeholder="Notes" value="<?php echo $data['Timetable']['notes'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10 btn-group">
                                    <button type="reset" class="btn btn-default">Reset</button>
                                    <button onclick="editTimetable()" class="btn btn-primary" style="margin-left: 10px">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript">
    function editTimetable() {
        var days = $('#inputDays').val();
        var start = $('#inputStart').val();
        var end = $('#inputEnd').val();
        var room = $('#inputRoom').val();
        var class_id = $('#inputClassId').val();
        var type = $('#inputType').val();
        var subject_id = $('#inputSubjectId').val();
        var subject_name = $('#inputSubjectName').val();
        var notes = $('#inputNotes').val();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '<?php echo Router::url(array('controller' => 'timetables', 'action' => 'edit'));?>',
            data: {
                'id': <?php echo $data['Timetable']['id']; ?>,
                'day': days,
                'start': start,
                'end': end,
                'room': room,
                'class_id': class_id,
                'type': type,
                'subject_id': subject_id,
                'subject_name': subject_name,
                'notes': notes
            },
            success: function(data) {
                console.log(data);
                if (data.result === 'true'){
                    $('#notifications').html('<p class=\"text-success\" style=\"margin-left: 15px; font-size: 200%;\">You have been edited record successfully!!!</p>');
                } else {
                    $('#notifications').html('<p class=\"text-warning\">Something went wrong. Please try again !!</p>');
                }
            }
        });
    }
</script>
    <?php } ?>
