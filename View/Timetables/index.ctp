<style type='text/css'>
    body {
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        margin: 0;
    }

    h1 {
        margin: 0 0 1em;
        padding: 0.5em;
    }

    p.description {
        font-size: 0.8em;
        padding: 1em;
        position: absolute;
        top: 3.2em;
        margin-right: 400px;
    }

    #message {
        font-size: 0.7em;
        position: absolute;
        top: 1em;
        right: 1em;
        width: 350px;
        display: none;
        padding: 1em;
        background: #ffc;
        border: 1px solid #dda;
    }
</style>

<?php echo $this->Html->css('week/jquery-ui-1.8.11.custom'); ?>
<?php echo $this->Html->css('week/jquery.weekcalendar'); ?>

<?php echo $this->Html->script('week/date'); ?>
<?php echo $this->Html->script('week/jquery.weekcalendar'); ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">My Timetables</h1>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Timetables
                <div class="pull-right">
                    <a href="/timetables/gridview" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                        View More Details
                    </a>
                    <a href="/timetables/add" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                        Add New
                    </a>
                </div>
            </div>
            <div class="panel-body" id="timetable-content">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
<?php
    $passEvent = [];
    foreach ($datas as $data){
        $a = $data['Timetable']['start'];
        $splitStart = explode(":", $data['Timetable']['start']);
        $splitEnd = explode(":", $data['Timetable']['end']);
        if ($data['Timetable']['day'] != 8){
            $newEvent = [$data['Timetable']['day'] - 1, $splitStart[0], $splitStart[1], $splitEnd[0], $splitEnd[1], $data['Timetable']['subject_name']];
            array_push($passEvent, $newEvent);
        } else  {
            $newEvent = [$data['Timetable']['day'] - 8, $splitStart[0], $splitStart[1], $splitEnd[0], $splitEnd[1], $data['Timetable']['subject_name']];
            array_push($passEvent, $newEvent);
        }
    }
    //handle date in php to generate event by week
    $curDate = date('w,d,m,o');
    $splitCurDate = explode(',' , $curDate);

    $stringToJS = "{ events : [";
    $count = 0;
    foreach ($passEvent as $eachData){
        $dateEvent = $splitCurDate[1] - ($splitCurDate[0] - $eachData[0]);
        $stringToJS .= "{'id':" . $count .", 'start': new Date(year, month, " . $dateEvent .", " . $eachData[1] . ", " . $eachData[2] ." ), 'end': new Date(year, month, " . $dateEvent .", " . $eachData[3] .", " . $eachData[4] ."),'title':'" . $eachData[5] ."'},";
        $count++;
    }
    $stringToJS .= "]
    }";
    $result = json_encode($passEvent);
?>
<script type='text/javascript'>
    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    var day = new Date().getDate();

    var eventDataPass = '<?php echo $result;?>';
    var newEvent = JSON.parse(eventDataPass);

    var eventData = <?php echo $stringToJS; ?>;
//    var eventData = {
//        events : [
//            {'id':1, 'start': new Date(year, month, day, 12), 'end': new Date(year, month, day, 13, 35),'title':'Nhập môn CNPM'},
//            {'id':2, 'start': new Date(year, month, day, 13, 20), 'end': new Date(year, month, day, 14, 45),'title':'Lập trình mạng'},
//            {'id':3, 'start': new Date(year, month, day + 1, 18), 'end': new Date(year, month, day + 1, 18, 45),'title':'Hướng đối tượng'},
//            {'id':4, 'start': new Date(year, month, day - 1, 8), 'end': new Date(year, month, day - 1, 9, 30),'title':'Project 2'},
//            {'id':5, 'start': new Date(year, month, day + 1, 14), 'end': new Date(year, month, day + 1, 15),'title':'Thiết kế xây dựng phần mềm'}
//        ]
//    };

    $(document).ready(function($) {
        $('#calendar').weekCalendar({
            timeslotsPerHour: 6,
            timeslotHeigh: 30,
            hourLine: true,
            data: eventData,
            height: function($calendar) {
                return $(window).height() - $('h1').outerHeight(true);
            },
            eventRender : function(calEvent, $event) {
                if (calEvent.end.getTime() < new Date().getTime()) {
                    $event.css('backgroundColor', '#aaa');
                    $event.find('.time').css({'backgroundColor': '#999', 'border':'1px solid #888'});
                }
            },
            eventNew: function(calEvent, $event) {
                displayMessage('<strong>Added event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
//                alert('You\'ve added a new event. You would capture this event, add the logic for creating a new event with your own fields, data and whatever backend persistence you require.');
            },
            eventDrop: function(calEvent, $event) {
                displayMessage('<strong>Moved Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
            },
            eventResize: function(calEvent, $event) {
                displayMessage('<strong>Resized Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
            },
            eventClick: function(calEvent, $event) {
                displayMessage('<strong>Clicked Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
            },
            eventMouseover: function(calEvent, $event) {
                displayMessage('<strong>Mouseover Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
            },
            eventMouseout: function(calEvent, $event) {
                displayMessage('<strong>Mouseout Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
            },
            noEvents: function() {
                displayMessage('There are no events for this week');
            }
        });

        function displayMessage(message) {
            $('#message').html(message).fadeIn();
        }

//        $('<div id="message" class="ui-corner-all"></div>').prependTo($('body'));
    });

</script>
