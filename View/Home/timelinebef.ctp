<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Economica'); ?>
<?php echo $this->Html->css('responsive-calendar'); ?>

<?php echo $this->Html->script('responsive-calendar'); ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Calender</h1>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-clock-o fa-fw"></i> Calender
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="responsive-calendar">
            <div class="controls">
                <a class="pull-left" data-go="prev"><div class="btn btn-primary">Prev</div></a>
                <h4><span data-head-year></span> <span data-head-month></span></h4>
                <a class="pull-right" data-go="next"><div class="btn btn-primary">Next</div></a>
            </div><hr/>
            <div class="day-headers">
                <div class="day header">Mon</div>
                <div class="day header">Tue</div>
                <div class="day header">Wed</div>
                <div class="day header">Thu</div>
                <div class="day header">Fri</div>
                <div class="day header">Sat</div>
                <div class="day header">Sun</div>
            </div>
            <div class="days" data-group="days">

            </div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
<script type="text/javascript">
    $(document).ready(function ($) {
        $(".responsive-calendar").responsiveCalendar({
            time: '2017-11',
            events: {
                "2017-11-27": {"number": 5, "url": "http://localhost/todolist"},
                "2017-11-26": {"number": 10},
                "2017-10-03": {"number": 1},
                "2017-12-12": {}}
        });
    });
</script>