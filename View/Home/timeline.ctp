    <?php echo $this->Html->css('fullcalendar.min'); ?>
    <?php echo $this->Html->css('fullcalendar.print.min'); ?>

    <?php echo $this->Html->script('weekcalender/moment.min'); ?>
    <?php echo $this->Html->script('weekcalender/fullcalendar.min'); ?>
    <script>
        $(document).ready(function($) {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                defaultDate: '2017-12-12',
                navLinks: true, // can click day/week names to navigate views

                weekNumbers: true,
                weekNumbersWithinDays: true,
                weekNumberCalculation: 'ISO',

                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: [
                    {
                        title: 'All Day Event',
                        start: '2017-12-01'
                    },
                    {
                        title: 'Long Event',
                        start: '2017-12-07',
                        end: '2017-12-10'
                    },
                    {
                        id: 999,
                        title: 'Repeating Event',
                        start: '2017-12-09T16:00:00'
                    },
                    {
                        id: 999,
                        title: 'Repeating Event',
                        start: '2017-12-16T16:00:00'
                    },
                    {
                        title: 'Conference',
                        start: '2017-12-11',
                        end: '2017-12-13'
                    },
                    {
                        title: 'Meeting',
                        start: '2017-12-12T10:30:00',
                        end: '2017-12-12T12:30:00'
                    },
                    {
                        title: 'Lunch',
                        start: '2017-12-12T12:00:00'
                    },
                    {
                        title: 'Meeting',
                        start: '2017-12-12T14:30:00'
                    },
                    {
                        title: 'Happy Hour',
                        start: '2017-12-12T17:30:00'
                    },
                    {
                        title: 'Dinner',
                        start: '2017-12-12T20:00:00'
                    },
                    {
                        title: 'Birthday Party',
                        start: '2017-12-13T07:00:00'
                    },
                    {
                        title: 'Click for Google',
                        url: 'http://google.com/',
                        start: '2017-12-28'
                    }
                ]
            });

        });

    </script>
    <style>
        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
    </style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Calender</h1>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-clock-o fa-fw"></i> Calender
    </div>
    <div class="panel-body">
        <div id='calendar'></div>
    </div>
</div>
