<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Add New Notes</h3>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tasks fa-fw"></i> New Notes
                <div class="pull-right">
                    <a href="/notes/index" class="btn btn-info active" role="button" style="margin-top: -7px;">
                        View Notes
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputName" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputContent" class="col-sm-2 control-label">Content</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputContent" placeholder="Content">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDate" class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="inputDate" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDateType" class="col-sm-2 control-label">Date Type</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputDateType" placeholder="Date Type">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNoteType" class="col-sm-2 control-label">Note Type</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputNoteType" placeholder="Note Type">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputFromTime" class="col-sm-2 control-label">From Time</label>
                        <div class="col-sm-10">
                            <input type="time" class="form-control" id="inputFromTime" placeholder="From Time">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEndTime" class="col-sm-2 control-label">End Time</label>
                        <div class="col-sm-10">
                            <input type="time" class="form-control" id="inputEndTime" placeholder="End Time">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPriority" class="col-sm-2 control-label">Priority</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPriority" placeholder="Priority">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 btn-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button onclick="addNewNotes()" class="btn btn-primary" style="margin-left: 10px">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function addNewNotes() {
        var name = $('#inputName').val();
        var content = $('#inputContent').val();
        var date = $('#inputDate').val();
        var date_type = $('#inputDateType').val();
        var from_time = $('#inputFromTime').val();
        var end_time = $('#inputEndTime').val();
        var priority = $('#inputPriority').val();
        var note_type = $('#inputNoteType').val();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '<?php echo Router::url(array('controller' => 'notes', 'action' => 'add'));?>',
            data: {
                'name': name,
                'content': content,
                'date': date,
                'date_type': date_type,
                'from_time': from_time,
                'end_time': end_time,
                'priority': priority,
                'note_type': note_type,
                'is_done': false
            },
            success: function(data) {
                if (data.result === 'true'){
                    $('#notifications').html('<p class=\"text-success\" style=\"margin-left: 15px; font-size: 200%;\">You have been added record successfully!!!</p>');
                } else {
                    $('#notifications').html('<p class=\"text-warning\">Something went wrong. Please try again !!</p>');
                }
            }
        });
    }
</script>
