<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">My Notes</h1>
    </div>
</div>
<div class="row" id="notifications"></div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Notes Of Mine
                <div class="pull-right">
                    <div class="panel-button">
                        <a href="#" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                            View All Notes
                        </a>
                        <a href="/notes/add" class="btn btn-primary active" role="button" style="margin-top: -7px;">
                            Add New Notes
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Today Notes
            </div>
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Test Mid-Term
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a>
                                    </li>
                                    <li><a href="#">Another action</a>
                                    </li>
                                    <li><a href="#">Something else here</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <p class="text-danger">Test Mid-Term for Network Programming</p>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Week Events
            </div>
            <div class="panel-body">

            </div>
        </div>
    </div>
</div>
