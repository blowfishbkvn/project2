<div class="panel-heading">
    <h3 class="panel-title">Please Sign In</h3>
</div>
<div class="panel-body">
    <?php echo $this->Flash->render('Auth'); ?>
    <?php echo $this->Form->create('User'); ?>
    <form role="form">
        <fieldset>
            <div class="form-group">
                <?php echo $this->Form->input('username', array('label' => false, 'placeholder' => 'Username', 'class' => 'form-control', 'rows' => '1')); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password', array('label' => false, 'placeholder' => 'Password', 'class' => 'form-control')); ?>
            </div>
            <div class="form-group">
            <?php echo $this->Form->input('email', array('label' => false, 'placeholder' => 'Email', 'class' => 'form-control')); ?>
            </div>
            <?php echo $this->Form->end(__('Sign Up', array('class' => 'submit btn btn-primary' ))); ?>
        </fieldset>
    </form>
    <div><?php echo $this->Html->link('Login', array('action' => 'login')); ?></div>
</div>



