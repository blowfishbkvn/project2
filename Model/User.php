<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

	class User extends AppModel{

		public $validate = array(
			'username' => array(
            	'rule' => 'notBlank'
        	),
        	'password' => array(
            	'rule' => 'notBlank'
        	),
        	'email' => array(
				'rule' => 'notBlank',
				'email'
			)
			);
	}
